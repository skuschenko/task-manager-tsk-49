package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class UserLogoutCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "logout current user";

    @NotNull
    private static final String NAME = "logout";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        serviceLocator.getSessionEndpoint().closeSession(session);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
