package com.tsc.skuschenko.tm.endpoint;

import com.tsc.skuschenko.tm.api.endpoint.ISessionEndpoint;
import com.tsc.skuschenko.tm.api.service.IServiceLocator;
import com.tsc.skuschenko.tm.api.service.dto.ISessionDTOService;
import com.tsc.skuschenko.tm.dto.SessionDTO;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Nullable
    private ISessionDTOService sessionService;

    public SessionEndpoint() {
        super(null);
    }

    public SessionEndpoint(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
        this.sessionService = serviceLocator.getSessionDTOService();
    }

    @WebMethod
    @Override
    public void closeSession(
            @WebParam(name = "session", partName = "session")
            @NotNull SessionDTO session
    ) throws AccessForbiddenException {
        serviceLocator.getSessionDTOService().validate(session);
        serviceLocator.getSessionDTOService().close(session);
    }

    @WebMethod
    @Nullable
    @Override
    public SessionDTO openSession(
            @WebParam(name = "login", partName = "login")
            @NotNull final String login,
            @WebParam(name = "password", partName = "password")
            @NotNull final String password
    ) {
        return serviceLocator.getSessionDTOService().open(login, password);
    }

}
