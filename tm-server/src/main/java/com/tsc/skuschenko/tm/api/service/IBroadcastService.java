package com.tsc.skuschenko.tm.api.service;

import org.jetbrains.annotations.NotNull;

import javax.jms.JMSException;

public interface IBroadcastService {
    void sendJmsMessage(
            @NotNull String message, @NotNull String className
    ) throws JMSException;

}
