package com.tsc.skuschenko.tm.listener;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tsc.skuschenko.tm.api.service.IBroadcastService;
import com.tsc.skuschenko.tm.dto.AbstractWrapper;
import com.tsc.skuschenko.tm.service.BroadcastService;
import com.tsc.skuschenko.tm.util.SystemUtil;
import org.jetbrains.annotations.NotNull;

import javax.jms.JMSException;
import javax.persistence.*;
import java.util.Date;

public class EntityListener {

    @NotNull
    private final static String POST_LOAD = "Post Load";

    @NotNull
    private final static String POST_PERSIST = "Post Persist";

    @NotNull
    private final static String POST_REMOVE = "Post Remove";

    @NotNull
    private final static String POST_UPDATE = "Post Update";

    @NotNull
    private final static String PRE_PERSIST = "Pre Persist";

    @NotNull
    private final static String PRE_REMOVE = "Pre Remove";

    @NotNull
    private final static String PRE_UPDATE = "Pre Update";

    @NotNull
    private final IBroadcastService broadcastService = new BroadcastService();

    @PostLoad
    public void postLoad(@NotNull Object entity)
            throws JsonProcessingException, JMSException {
        sendMessage(entity, POST_LOAD);
    }

    @PostPersist
    public void postPersist(@NotNull Object entity)
            throws JsonProcessingException, JMSException {
        sendMessage(entity, POST_PERSIST);
    }

    @PostRemove
    public void postRemove(@NotNull Object entity)
            throws JsonProcessingException, JMSException {
        sendMessage(entity, POST_REMOVE);
    }

    @PostUpdate
    public void postUpdate(@NotNull Object entity)
            throws JsonProcessingException, JMSException {
        sendMessage(entity, POST_UPDATE);
    }

    @PrePersist
    public void prePersist(@NotNull Object entity)
            throws JsonProcessingException, JMSException {
        sendMessage(entity, PRE_PERSIST);
    }

    @PreRemove
    public void preRemove(@NotNull Object entity)
            throws JsonProcessingException, JMSException {
        sendMessage(entity, PRE_REMOVE);
    }

    @PreUpdate
    public void preUpdate(@NotNull Object entity)
            throws JsonProcessingException, JMSException {
        sendMessage(entity, PRE_UPDATE);
    }

    private void sendMessage(
            @NotNull final Object entity,
            @NotNull final String message
    ) throws JsonProcessingException, JMSException {
        @NotNull AbstractWrapper abstractWrapper = new AbstractWrapper(
                entity, SystemUtil.getCurrentDateTime(),
                entity.getClass().getSimpleName(), message
        );
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json =
                objectMapper.writeValueAsString(abstractWrapper);
        broadcastService.sendJmsMessage(
                json, abstractWrapper.getClassName()
        );
    }

}
