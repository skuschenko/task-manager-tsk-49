package com.tsc.skuschenko.tm.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@Getter
@XmlRootElement
@AllArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class AbstractWrapper {

    @Nullable
    private final Object entity;

    @Nullable
    private final String date;

    @Nullable
    private final String className;

    @Nullable
    private final String actionName;

}
