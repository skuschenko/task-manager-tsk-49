package com.tsc.skuschenko.tm.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.tsc.skuschenko.tm.listener.EntityListener;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;

@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@Entity
@NoArgsConstructor
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_session")
@EntityListeners(EntityListener.class)
public class SessionDTO extends AbstractEntityDTO implements Cloneable {

    @Nullable
    private String signature;

    @Nullable
    private Long timestamp;

    @Column(name = "user_id")
    @NotNull
    private String userId;

    @Nullable
    @Override
    public SessionDTO clone() {
        try {
            return (SessionDTO) super.clone();
        } catch (CloneNotSupportedException e) {
            return null;
        }
    }

}
