package com.tsc.skuschenko.tm.service;


import com.tsc.skuschenko.tm.api.service.IBroadcastService;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.jetbrains.annotations.NotNull;

import javax.jms.*;

public class BroadcastService implements IBroadcastService {

    @Override
    public void sendJmsMessage(
            @NotNull final String message, @NotNull final String className
    ) throws JMSException {
        @NotNull final ConnectionFactory connectionFactory
                = new ActiveMQConnectionFactory(
                ActiveMQConnectionFactory.DEFAULT_BROKER_URL
        );
        @NotNull final Connection connection =
                connectionFactory.createConnection();
        connection.start();
        @NotNull final javax.jms.Session session = connection.createSession(
                false, javax.jms.Session.AUTO_ACKNOWLEDGE
        );
        @NotNull final Destination destination = session.createTopic(className);
        @NotNull MessageProducer messageProducer =
                session.createProducer(destination);
        @NotNull final TextMessage textMessage =
                session.createTextMessage(message);
        messageProducer.send(textMessage);
        connection.close();
    }

}
